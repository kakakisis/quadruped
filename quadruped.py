import argparse
import math
import pybullet as p
from time import sleep
from scipy.interpolate import interp1d

dt = 0.01
FEET_ONE = [0,1,2]
FEET_TWO = [3,4,5]
FEET_THREE = [6,7,8]
FEET_FOUR = [9,10,11]

L1 = 4.0
L2 = 4.5
L3 = 6.5
L4 = 8.7

def init():
    """Initialise le simulateur

    Returns:
        int -- l'id du robot
    """
    # Instanciation de Bullet
    physicsClient = p.connect(p.GUI)
    p.setGravity(0, 0, -10)

    # Chargement du sol
    planeId = p.loadURDF('plane.urdf')

    # Chargement du robot
    startPos = [0, 0, 0.1]
    startOrientation = p.getQuaternionFromEuler([0, 0, 0])
    robot = p.loadURDF("./quadruped/robot.urdf",
                        startPos, startOrientation)

    p.setPhysicsEngineParameter(fixedTimeStep=dt)
    return robot

def setJoints(robot, joints):
    """Definis les angles cibles pour les moteurs du robot

    Arguments:
        int -- identifiant du robot
        joints {list} -- liste des positions cibles (rad)
    """
    jointsMap = [0, 1, 2, 4, 5, 6, 8, 9, 10, 12, 13, 14]
    for k in range(len(joints)):
        jointInfo = p.getJointInfo(robot, jointsMap[k])
        p.setJointMotorControl2(robot, jointInfo[0], p.POSITION_CONTROL, joints[k])

def demo(t, amplitude):
    """Demonstration de mouvement (fait osciller une patte)

    Arguments:
        t {float} -- Temps ecoule depuis le debut de la simulation

    Returns:
        list -- les 12 positions cibles (radian) pour les moteurs
        float -- amplitude de l'oscillation
    """
    joints = [0]*12
    joints[0] = math.sin(t) * amplitude
    return joints

#fonction pour donner un point a la premiere patte
def one_ik(joints, x, y, z):
        # premier moteur
        theta_0 = math.atan2(y,x)
        joints[FEET_ONE[0]] = theta_0

        #troisieme moteur
        A = [L2 * math.cos(theta_0), L2 * math.sin(theta_0), 0]
        AM = ( (x - A[0])**2 + (y - A[1])**2 + (z - A[2])**2 )**0.5
        ac = (L3**2 + L4**2 - AM**2) / (2*L3*L4)
        #on ramene entre -1 et 1
        if(ac < -1):
            ac = -1
        if(ac > 1):
            ac = 1

        theta_2 = math.acos(ac)
        #math_pi pour ramener le moteur sur la position "de base"
        joints[FEET_ONE[2]] = math.pi - theta_2

        #deuxieme moteur
        ac_2 = (math.pow(AM,2) + math.pow(L3,2) - math.pow(L4,2)) / (2*AM*L3)
        #on ramene entre -1 et 1
        if(ac_2 < -1):
            ac_2 = -1
        if(ac_2 > 1):
            ac_2 = 1

        alpha = math.acos(ac_2)
        beta = math.asin(z / AM)
        theta_1 = math.pi - (beta + alpha)
        #math_pi pour ramener le moteur sur la position "de base"
        joints[FEET_ONE[1]] = math.pi - theta_1

        return joints

#idem pour la deuxieme patte..
def two_ik(joints, x, y, z):
        # premier moteur
        theta_0 = math.atan2(y,x)
        joints[FEET_TWO[0]] = theta_0

        #troisieme moteur
        A = [L2 * math.cos(theta_0), L2 * math.sin(theta_0), 0]
        AM = ( (x - A[0])**2 + (y - A[1])**2 + (z - A[2])**2 )**0.5
        ac = (L3**2 + L4**2 - AM**2) / (2*L3*L4)
        #on ramene entre -1 et 1
        if(ac < -1):
            ac = -1
        if(ac > 1):
            ac = 1

        theta_2 = math.acos(ac)
        #math_pi pour ramener le moteur sur la position "de base"
        joints[FEET_TWO[2]] = math.pi - theta_2

        #deuxieme moteur
        ac_2 = (math.pow(AM,2) + math.pow(L3,2) - math.pow(L4,2)) / (2*AM*L3)
        #on ramene entre -1 et 1
        if(ac_2 < -1):
            ac_2 = -1
        if(ac_2 > 1):
            ac_2 = 1

        alpha = math.acos(ac_2)
        beta = math.asin(z / AM)
        theta_1 = math.pi - (beta + alpha)
        #math_pi pour ramener le moteur sur la position "de base"
        joints[FEET_TWO[1]] = math.pi - theta_1

        return joints

#etc...
def three_ik(joints, x, y, z):
        # premier moteur
        theta_0 = math.atan2(y,x)
        joints[FEET_THREE[0]] = theta_0

        #troisieme moteur
        A = [L2 * math.cos(theta_0), L2 * math.sin(theta_0), 0]
        AM = ( (x - A[0])**2 + (y - A[1])**2 + (z - A[2])**2 )**0.5
        ac = (L3**2 + L4**2 - AM**2) / (2*L3*L4)
        #on ramene entre -1 et 1
        if(ac < -1):
            ac = -1
        if(ac > 1):
            ac = 1

        theta_2 = math.acos(ac)
        #math_pi pour ramener le moteur sur la position "de base"
        joints[FEET_THREE[2]] = math.pi - theta_2

        #deuxieme moteur
        ac_2 = (math.pow(AM,2) + math.pow(L3,2) - math.pow(L4,2)) / (2*AM*L3)
        #on ramene entre -1 et 1
        if(ac_2 < -1):
            ac_2 = -1
        if(ac_2 > 1):
            ac_2 = 1

        alpha = math.acos(ac_2)
        beta = math.asin(z / AM)
        theta_1 = math.pi - (beta + alpha)
        #math_pi pour ramener le moteur sur la position "de base"
        joints[FEET_THREE[1]] = math.pi - theta_1

        return joints

def four_ik(joints, x, y, z):
        # premier moteur
        theta_0 = math.atan2(y,x)
        joints[FEET_FOUR[0]] = theta_0

        #troisieme moteur
        A = [L2 * math.cos(theta_0), L2 * math.sin(theta_0), 0]
        AM = ( (x - A[0])**2 + (y - A[1])**2 + (z - A[2])**2 )**0.5
        ac = (L3**2 + L4**2 - AM**2) / (2*L3*L4)
        #on ramene entre -1 et 1
        if(ac < -1):
            ac = -1
        if(ac > 1):
            ac = 1

        theta_2 = math.acos(ac)
        #math_pi pour ramener le moteur sur la position "de base"
        joints[FEET_FOUR[2]] = math.pi - theta_2

        #deuxieme moteur
        ac_2 = (math.pow(AM,2) + math.pow(L3,2) - math.pow(L4,2)) / (2*AM*L3)
        #on ramene entre -1 et 1
        if(ac_2 < -1):
            ac_2 = -1
        if(ac_2 > 1):
            ac_2 = 1

        alpha = math.acos(ac_2)
        beta = math.asin(z / AM)
        theta_1 = math.pi - (beta + alpha)
        #math_pi pour ramener le moteur sur la position "de base"
        joints[FEET_FOUR[1]] = math.pi - theta_1

        return joints

#fonction du mode
def leg_ik(t, x, y, z):
    if(t < 1):
        return stable()
    else:
        joints = [0] * 12

        # premier moteur
        theta_0 = math.atan2(y,x)
        joints[0] = theta_0

        #troisieme moteur
        A = [L2 * math.cos(theta_0), L2 * math.sin(theta_0), 0]
        AM = ( (x - A[0])**2 + (y - A[1])**2 + (z - A[2])**2 )**0.5
        ac = (L3**2 + L4**2 - AM**2) / (2*L3*L4)
        #on ramene entre -1 et 1
        if(ac < -1):
            ac = -1
        if(ac > 1):
            ac = 1

        theta_2 = math.acos(ac)
        #math_pi pour ramener le moteur sur la position "de base"
        joints[2] = math.pi - theta_2

        #deuxieme moteur
        ac_2 = (math.pow(AM,2) + math.pow(L3,2) - math.pow(L4,2)) / (2*AM*L3)
        #on ramene entre -1 et 1
        if(ac_2 < -1):
            ac_2 = -1
        if(ac_2 > 1):
            ac_2 = 1

        alpha = math.acos(ac_2)
        beta = math.asin(z / AM)
        theta_1 = math.pi - (beta + alpha)
        #math_pi pour ramener le moteur sur la position "de base"
        joints[1] = math.pi - theta_1


        return joints

def fun(t):
    epsilon = 1
    if(t < 1):
        return stable()
    if(math.ceil(t) % 2 == 0.0):
        joints = [0] * 12
        one_ik(joints, L2+L3+epsilon,L2+L3+epsilon,-3)
        two_ik(joints, L2+L3+epsilon,L2+L3+epsilon,-3)
        three_ik(joints, L2+L3+epsilon,L2+L3+epsilon,-3)
        four_ik(joints, L2+L3+epsilon, L2+L3+epsilon,-3)
    else:
        joints = [0] * 12
        one_ik(joints, L2+L3+epsilon,-L2+L3+epsilon,-5)
        two_ik(joints, L2+L3+epsilon,-L2+L3+epsilon,-5)
        three_ik(joints, L2+L3+epsilon,-L2+L3+epsilon,-5)
        four_ik(joints, L2+L3+epsilon,-L2+L3+epsilon,-5)

    return joints

def robot_ik(t, x, y, z):
    joints = [0] * 12
    epsilon = 1
    premier_pied_pos = [L2+L3+epsilon,0,-2]
    deuxieme_pied_pos = [L2+L3+epsilon,0,-2]
    troisieme_pied_pos = [L2+L3+epsilon,0,-2]
    quatrieme_pied_pos = [L2+L3+epsilon,0,-2]

    if(t < 1):
        return stable()
    elif(t < 2):
        #position de "base"
        premier_pied_pos = [L2+L3+epsilon,0,-2]
        deuxieme_pied_pos = [L2+L3+epsilon,0,-2]
        troisieme_pied_pos = [L2+L3+epsilon,0,-2]
        quatrieme_pied_pos = [L2+L3+epsilon,0,-2]
    else:
        #on decale les x des pates
        premier_pied_pos[0] = premier_pied_pos[0] + x - y
        deuxieme_pied_pos[0] = deuxieme_pied_pos[0] - x - y
        troisieme_pied_pos[0] = troisieme_pied_pos[0] - x + y
        quatrieme_pied_pos[0] = quatrieme_pied_pos[0] + x + y
        #on decale les y des pates
        premier_pied_pos[1] = premier_pied_pos[1] - y - x
        deuxieme_pied_pos[1] = deuxieme_pied_pos[1] - x + y
        troisieme_pied_pos[1] = troisieme_pied_pos[1] + x + y
        quatrieme_pied_pos[1] = quatrieme_pied_pos[1] - y + x
        #on decale le z des pates
        premier_pied_pos[2] = premier_pied_pos[2] - z
        deuxieme_pied_pos[2] = deuxieme_pied_pos[2] - z
        troisieme_pied_pos[2] = troisieme_pied_pos[2] - z
        quatrieme_pied_pos[2] = quatrieme_pied_pos[2] - z

    # on met a jour les points
    one_ik(joints, premier_pied_pos[0],premier_pied_pos[1],premier_pied_pos[2])
    two_ik(joints, deuxieme_pied_pos[0],deuxieme_pied_pos[1],deuxieme_pied_pos[2])
    three_ik(joints, troisieme_pied_pos[0],troisieme_pied_pos[1],troisieme_pied_pos[2])
    four_ik(joints, quatrieme_pied_pos[0],quatrieme_pied_pos[1],quatrieme_pied_pos[2])

    # on renvoie
    return joints


def walk(t, x_sp, y_sp, r_sp):
    epsilon = 1
    pos_first = [L2+L3+epsilon,0,-2]

    avancee_droite = 4
    #pos premiere patte
    xs_first = [pos_first[0], pos_first[0], pos_first[0]+avancee_droite+x_sp, pos_first[0]+avancee_droite+x_sp,    pos_first[0],pos_first[0], pos_first[0], pos_first[0]]
    ys_first = [pos_first[1],pos_first[1],pos_first[1]+avancee_droite+y_sp, pos_first[1]+avancee_droite+y_sp,    pos_first[1],pos_first[1],pos_first[1],pos_first[1]]
    zs_first = [-4,2,2,-4,     -4,-4,-4,-4]

    #pos quatrieme patte
    xs_four = [pos_first[0], pos_first[0], pos_first[0]-avancee_droite-x_sp, pos_first[0]-avancee_droite-x_sp,       pos_first[0],pos_first[0], pos_first[0], pos_first[0]]
    ys_four = [pos_first[1],pos_first[1],pos_first[1]+avancee_droite+y_sp,pos_first[1]+avancee_droite+y_sp,       pos_first[1],pos_first[1],pos_first[1],pos_first[1]]
    zs_four = [-4,2,2,-4,    -4,-4,-4,-4]


    avancee_gauche = 4.5
    #pos quatrieme patte
    xs_three = [pos_first[0], pos_first[0], pos_first[0], pos_first[0],       pos_first[0],pos_first[0], pos_first[0]-avancee_gauche-x_sp, pos_first[0]-avancee_gauche-x_sp]
    ys_three = [pos_first[1], pos_first[1], pos_first[1], pos_first[1],       pos_first[1],pos_first[1],pos_first[1]-avancee_gauche-y_sp,pos_first[1]-avancee_gauche-y_sp]
    zs_three = [-4,-4,-4,-4,    -4,2,2,-4]

    #pos deuxieme patte
    xs_second = [pos_first[0], pos_first[0], pos_first[0], pos_first[0],       pos_first[0],pos_first[0], pos_first[0]+avancee_gauche+x_sp, pos_first[0]+avancee_gauche+x_sp]
    ys_second = [pos_first[1], pos_first[1], pos_first[1], pos_first[1],       pos_first[1],pos_first[1],pos_first[1]-avancee_gauche-y_sp, pos_first[1]-avancee_gauche-y_sp]
    zs_second = [-4,-4,-4,-4,    -4,2,2,-4]

    #timeline (same for all axis, all legs)
    ts = [1, 1+1/r_sp, 1+2*(1/r_sp), 1+3*(1/r_sp),   1+4*(1/r_sp), 1+5*(1/r_sp), 1+6*(1/r_sp), 1+7*(1/r_sp)]

    if (t < 1):
        return stable()
    else:
        joints = [0] * 12
        interpy_first = interp1d(ts,ys_first)
        interpz_first = interp1d(ts,zs_first)
        interpx_first = interp1d(ts,xs_first)

        interpy_four = interp1d(ts,ys_four)
        interpz_four = interp1d(ts,zs_four)
        interpx_four = interp1d(ts,xs_four)

        interpy_three = interp1d(ts,ys_three)
        interpz_three = interp1d(ts,zs_three)
        interpx_three = interp1d(ts,xs_three)

        interpy_second = interp1d(ts,ys_second)
        interpz_second = interp1d(ts,zs_second)
        interpx_second = interp1d(ts,xs_second)

        if(t > 1 and t < ts[-1]):
            one_ik(joints, interpx_first(t), interpy_first(t), interpz_first(t))
            two_ik(joints, interpx_second(t), interpy_second(t), interpz_second(t))
            three_ik(joints, interpx_three(t), interpy_three(t), interpz_three(t))
            four_ik(joints, interpx_four(t), interpy_four(t), interpz_four(t))
        if(t > ts[-1]):
            one_ik(joints, interpx_first(ts[-1]), interpy_first(ts[-1]), interpz_first(ts[-1]))
            two_ik(joints, interpx_second(ts[-1]), interpy_second(ts[-1]), interpz_second(ts[-1]))
            three_ik(joints, interpx_three(ts[-1]), interpy_three(ts[-1]), interpz_three(ts[-1]))
            four_ik(joints, interpx_four(ts[-1]), interpy_four(ts[-1]), interpz_four(ts[-1]))

        return joints

def goto(t, x, y, t_radian):
    if (t < 1):
        return stable()
    else:
        joints = [0] * 12
        #todo: goto
        return stable()

def stable():
    joints = [0] * 12
    #first feet
    joints[1] = math.pi / 4
    joints[2] = math.pi / 2
    #second feet
    joints[4] = math.pi / 4
    joints[5] = math.pi / 2
    #third feet
    joints[7] = math.pi / 4
    joints[8] = math.pi / 2
    #fourth feet
    joints[10] = math.pi / 4
    joints[11] = math.pi / 2
    return joints

if __name__ == "__main__":
    # Arguments
    parser = argparse.ArgumentParser(prog="TD Robotique S8")
    parser.add_argument('-m', type=str, help='Mode', required=True)
    parser.add_argument('-x', type=float, help='X target for goto (m)', default=1.0)
    parser.add_argument('-y', type=float, help='Y target for goto (m)', default=0.0)
    parser.add_argument('-t', type=float, help='Theta target for goto (rad)', default=0.0)
    args = parser.parse_args()

    mode, x, y, t_rad = args.m, args.x, args.y, args.t


    #ajout des modes
    if mode not in ['stable', 'demo', 'leg_ik', 'robot_ik', 'walk', 'goto', 'fun']:
        print('Le mode %s est inconnu' % mode)
        exit(1)


    robot = init()

    #ajouter la presentation du mode ici
    if mode == 'stable':
        print('Position stable...')
    elif mode == 'demo':
        amplitude = p.addUserDebugParameter("amplitude", 0.1, 1, 0.3)
        print('Mode de demonstration...')
    elif mode == 'leg_ik':
        x = p.addUserDebugParameter("x", 0, L2+L3+L4 - 0.1, 0)
        y = p.addUserDebugParameter("y", -(L2+L3+L4), (L2+L3+L4) - 0.1, L2+L3+L4 - 0.1)
        z = p.addUserDebugParameter("z", -(L2+L3+L4), (L2+L3+L4) - 0.1, 0)
        print('Atteinte de point avec une patte...')
    elif mode == 'walk':
        x_sp = p.addUserDebugParameter("x speed",0.1, 1, 0.3)
        y_sp = p.addUserDebugParameter("y speed",0.1, 1, 0.3)
        t_sp = p.addUserDebugParameter("t (rotation) speed",0.1, 5, 1.5)
        print('Marche...')
    elif mode == 'robot_ik':
        x = p.addUserDebugParameter("x", -10, 10, 0)
        y = p.addUserDebugParameter("y", -10, 10, 0)
        z = p.addUserDebugParameter("z", -5, 5, 0)
        print('Atteinte de point avec le corps...')
    elif mode == 'goto':
        print('Atteinte de point avec la marche...')
    elif mode == 'fun':
        print("dance time")
    else:
        raise Exception('Mode non implemente: %s' % mode)

    t = 0
    timer = 0.0

    # Boucle principale
    while True:
        t += dt
        timer += dt

        if mode == 'demo':
            # Recuperation des positions cibles
            joints = demo(t, p.readUserDebugParameter(amplitude))

        #ajouter la logique du mode ici

        if mode == 'stable':
            joints = stable()

        if mode == 'leg_ik':
            joints = leg_ik(t,p.readUserDebugParameter(x),p.readUserDebugParameter(y),p.readUserDebugParameter(z))

        if mode == 'walk':
            joints = walk(timer,p.readUserDebugParameter(x_sp),p.readUserDebugParameter(y_sp),p.readUserDebugParameter(t_sp))
            timer_end = 1 + 7 * (1 / p.readUserDebugParameter(t_sp))
            if(timer > timer_end):
                timer = 1.0

        if mode == 'robot_ik':
            joints = robot_ik(t,p.readUserDebugParameter(x),p.readUserDebugParameter(y),p.readUserDebugParameter(z))

        if mode == 'goto':
            joints = goto(t, x, y, t_rad)

        if mode == 'fun':
            joints = fun(t)

        # Envoi des positions cibles au simulateur
        setJoints(robot, joints)

        # Mise a jour de la simulation
        p.stepSimulation()
        sleep(dt)
